<?php
if ( isset( $_SERVER['IDE_CODECEPTION_EXE'] ) || 'codecept' === basename( $_SERVER['PHP_SELF'] ) ) {
	class_alias( '\ComposePress\Core\\v0_10_1_0\\Abstracts\\Component', '\\Framework\\Component' );
	class_alias( '\ComposePress\Core\\v0_10_1_0\\Abstracts\\Plugin', '\\Framework\\Plugin' );
} else {
	class_alias( '\ComposePress\Core\\v0_10_1_0\\Abstracts\\Component', '\\ComposePress\\Views\\v0_5_0_0\\Framework\\Component', false );
}
