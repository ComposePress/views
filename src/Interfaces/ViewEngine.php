<?php


namespace Interfaces;
/**
 * Interface ViewEngine
 *
 * @package ComposePress\Views\Interfaces
 * @property string $file_extension
 * @property bool   $buffered
 */
interface ViewEngine {

	/**
	 * @param string $file
	 *
	 * @return bool
	 */
	public function is_overridable( $file );

	/**
	 * @return string
	 */
	public function get_file_extension();

	/**
	 * @return bool
	 */
	public function is_buffered();

	/**
	 * @param string $view
	 * @param array  $data
	 * @param bool   $return
	 *
	 * @return bool
	 */
	public function render( $view, $data = array(), $return = false );
}
