<?php


namespace Exceptions;


/**
 * Class ViewException
 *
 * @package ComposePress\Views\Exceptions
 */
class ViewException extends \Exception {

}
