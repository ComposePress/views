<?php

use Exceptions\ViewException;
use Framework\Component;
use Interfaces\ViewEngine;

/**
 * Class View_0_5_0_0
 *
 * @property $view_path
 * @property $overridable_path
 */
class View extends Component {
	/**
	 * @var string
	 */
	protected $view_path;
	/**
	 * @var string
	 */
	protected $view;
	/**
	 * @var string
	 */
	protected $view_file;
	/**
	 * @var string
	 */
	protected $view_file_full;
	/**
	 * @var Interfaces\ViewEngine
	 */
	private $view_engine;

	public function __construct( ViewEngine $view_engine ) {
		$this->view_engine = $view_engine;
	}

	/**
	 * @return string
	 */
	public function get_view_path() {
		return $this->view_path;
	}

	/**
	 * @param string $view_path
	 */
	public function set_view_path( $view_path ) {
		$this->view_path = trailingslashit( $view_path );
	}

	/**
	 * @return bool
	 */
	public function setup() {
		$this->view_path = untrailingslashit( dirname( $this->plugin->plugin_file ) ) . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR;
		if ( ! $this->is_component( $this->view_engine ) ) {
			throw new ViewException( 'View Engine must be a component.' );
		}

		return true;
	}

	/**
	 * Render the view
	 *
	 * @param string                  $view
	 * @param array<string|int,mixed> $data
	 * @param bool                    $return
	 *
	 * @return void|string
	 * @throws \Exception
	 */
	public function render( $view, $data = array(), $return = false ) {
		if ( ! is_array( $data ) ) {
			throw new ViewException( 'Data must an array for rendering a view.' );
		}
		$data = array_merge( $data, [ 'view' => $this ] );
		$this->compute_paths( $view );
		if ( ! $this->plugin->wp_filesystem->is_file( $this->view_file_full ) ) {
			throw new ViewException( sprintf( 'View %s does not exist!', $view ) );
		}
		if ( $return && $this->view_engine->buffered ) {
			ob_start();
		}
		$located_view = $this->locate();
		$output       = $this->view_engine->render( $located_view, $data, $return );
		if ( $return ) {
			if ( $this->view_engine->buffered ) {
				/**
				 * @noinspection PhpInconsistentReturnPointsInspection
				 */
				return ob_get_clean();
			}

			return $output;
		}
	}

	/**
	 * @param string $view
	 */
	protected function compute_paths( $view ) {
		$this->view           = $view;
		$this->view_file      = $this->view_path . rtrim( $this->view, '/\\' );
		$this->view_file_full = $this->view_file . '.' . $this->view_engine->file_extension;
	}

	/**
	 */
	public function locate( $view = null ) {
		if ( $view ) {
			$this->compute_paths( $view );
		}
		$overridable = $this->is_overridable();
		$found       = false;
		if ( $overridable ) {
			$default = $this->view_file_full;
			$views   = (array) apply_filters( "{$this->plugin->safe_slug}_get_view", $default, $this->view . '.' . $this->view_engine->file_extension, $this->view );
			$views   = array_diff( [ $default ], $views );
			foreach ( $views as $view_item ) {
				if ( $this->plugin->wp_filesystem->is_file( $view_item ) ) {
					$found = $view_item;
					break;
				}
			}
			if ( ! $found ) {
				$found = locate_template( $this->get_overridable_view(), false, false );
			}
		}
		if ( ! $found ) {
			$found = $this->view_file_full;
		}

		return $found;
	}

	/**
	 * @return bool
	 */
	protected function is_overridable() {
		return $this->view_engine->is_overridable( $this->view_file_full );
	}

	/**
	 * @param null|string $view
	 *
	 * @return string
	 */
	public function get_overridable_view( $view = null ) {
		if ( $view ) {
			$this->compute_paths( $view );
		}

		return $this->get_overridable_path() . $this->view . '.' . $this->view_engine->get_file_extension();
	}

	/**
	 * @param $view
	 *
	 * @return string
	 */
	public function get_overridable_path() {
		return $this->plugin->safe_slug . DIRECTORY_SEPARATOR;
	}

	/**
	 * @return \ComposePress\Views\Interfaces\ViewEngine
	 */
	public function get_view_engine() {
		return $this->view_engine;
	}

	/**
	 * @param \ComposePress\Views\Interfaces\ViewEngine $view_engine
	 */
	public function set_view_engine( $view_engine ) {
		$this->view_engine = $view_engine;
	}
}
