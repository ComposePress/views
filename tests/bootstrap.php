<?php
namespace {

	use ComposePress\Dice\Dice;

	require_once __DIR__ . '/../vendor/autoload.php';

	if ( ! class_exists( 'PHPUnit_Framework_Assert' ) ) {
		class_alias( 'PHPUnit\Framework\Assert', 'PHPUnit_Framework_Assert' );
	}

	if ( ! class_exists( 'PHPUnit_Framework_TestCase' ) ) {
		class_alias( 'PHPUnit\Framework\TestCase', 'PHPUnit_Framework_TestCase' );
	}

	if ( ! class_exists( 'PHPUnit_Framework_Exception' ) ) {
		class_alias( 'PHPUnit\Framework\Exception', 'PHPUnit_Framework_Exception' );
	}

	/**
	 * @param string $env
	 *
	 * @return \ComposePress\Dice\Dice
	 */
	function test_plugin_container( $env = 'testing' ) {
		global $env_global;

		if ( ! empty( $env_global ) ) {
			$env = $env_global;
		}

		$container = new Dice();
		include __DIR__ . "/config_{$env}.php";

		return $container;
	}

	/**
	 * @return \Test_Plugin
	 */
	function test_plugin() {
		return test_plugin_container()->create( '\Test_Plugin' );
	}

	/**
	 * @return \Test_Plugin
	 */
	function test_plugin2() {
		return test_plugin_container( 'testing2' )->create( '\Test_Plugin' );
	}


	global $wp_filter;

	$wp_filter['plugins_url'][10]['modify_plugins_url']     = [
		'function'      => 'modify_plugins_url',
		'accepted_args' => 1,
	];
	$wp_filter['pre_option_siteurl'][10]['modify_site_url'] = [ 'function' => 'modify_site_url', 'accepted_args' => 1 ];
	$wp_filter['pre_option_home'][10]['modify_site_url']    = [ 'function' => 'modify_site_url', 'accepted_args' => 0 ];

	/**
	 * @param $url
	 *
	 * @return mixed
	 */
	function modify_plugins_url( $url ) {
		return str_replace( __DIR__, '/test-plugin', $url );
	}

	function modify_site_url() {
		return 'http://example.org';
	}

	error_reporting( E_ALL & ~E_WARNING );
}
