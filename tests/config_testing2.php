<?php

/* @var $container \ComposePress\Dice\Dice */

$container = $container->addRule( '\Test_Plugin', [
	'shared' => true,
] );
$container = $container->addRule( '\View',
	[
		'substitutions' => [
			'Interfaces\ViewEngine' => 'PhpEngineBare',
		],
	] );
