<?php


use Interfaces\ViewEngine;

class PhpEngineBare implements ViewEngine {

	private $overridable = false;

	public function is_overridable( $file ) {
		return $this->overridable;
	}

	/**
	 * @param bool $overridable
	 */
	public function set_overridable( $overridable ) {
		$this->overridable = $overridable;
	}

	public function get_file_extension() {
		return 'php';
	}

	public function is_buffered() {
		return true;
	}

	public function render( $view, $data = array(), $return = false ) {
		$this->wp_query->query_vars = array_merge( $this->wp_query->query_vars, $data );
		load_template( $view, false );
	}
}
