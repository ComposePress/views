<?php

class PhpEngineEcho extends \PhpEngine {

	public function is_buffered() {
		return false;
	}

	public function render( $view, $data = array(), $return = false ) {
		return $this->plugin->wp_filesystem->get_contents( $view );
	}
}
