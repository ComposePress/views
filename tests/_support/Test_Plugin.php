<?php


use Framework\Plugin;

class Test_Plugin extends Plugin {

	const PLUGIN_SLUG = 'test_plugin';

	/**
	 * @param bool $network_wide
	 *
	 * @return void
	 */
	public function activate( $network_wide ) {
		// TODO: Implement activate() method.
	}

	/**
	 * @param bool $network_wide
	 *
	 * @return  void
	 */
	public function deactivate( $network_wide ) {
		// TODO: Implement deactivate() method.
	}

	/**
	 * @return void
	 */
	public function uninstall() {
		// TODO: Implement uninstall() method.
	}
}
