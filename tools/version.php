#!/usr/bin/php
<?php

use Composer\Semver\VersionParser;
use Symfony\Component\Console\Input\ArrayInput;
use function Humbug\PhpScoper\create_application;

require_once __DIR__ . '/../vendor/autoload.php';

chdir( __DIR__ . '/../' );


$semver  = new VersionParser();
$version = $semver->normalize( $argv[1] );
$version = str_replace( '.', "_", $version );

$namespace_prefix = "ComposePress\Views\\v{$version}";
$input            = new ArrayInput( [
	'command' => 'add-prefix',
	'-p'      => $namespace_prefix,
	'-f'      => true,
] );

$app = create_application();
$app->setAutoExit( true );
$app->run( $input );
