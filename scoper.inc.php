<?php

declare( strict_types=1 );

/** @var Symfony\Component\Finder\Finder $finder */

if ( class_exists( 'Isolated\Symfony\Component\Finder\Finder' ) ) {
	$finder = 'Isolated\Symfony\Component\Finder\Finder';
} else {
	$finder = 'Symfony\Component\Finder\Finder';
}

return [
	'finders'         => [
		$finder::create()
		       ->files()
		       ->in( 'src' ),
	],
	'files-whitelist' => [
		'src/framework.php',
	],
];
